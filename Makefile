PROG1 := rgb2gray_bandwidth
CU1   := rgb2gray_bandwidth.cu
OBJ1  := $(CU1:.cu=.o)

ARCH := $(shell uname -m)

CUDA_ROOT := /usr/local/cuda
CUDA_COMMON_INC_DIR := $(CUDA_ROOT)/samples/common/inc
CUDA_COMMON_LIB_DIR := $(CUDA_ROOT)/samples/common/lib/linux/$(ARCH)
CUDA_ARCH := 50 52 53 61 70
CUDA_LIB_DIR :=

NVCC := $(CUDA_ROOT)/bin/nvcc

ifeq ($(ARCH),armv7l)
CUDA_LIB_DIR += $(CUDA_ROOT)/lib
else
CUDA_LIB_DIR += $(CUDA_ROOT)/lib64
endif

NVCCFLAGS    := -std=c++11 -O3 -DNDEBUG -Wno-deprecated-gpu-targets
NVCC_LDFLAGS := -L$(CUDA_LIB_DIR) -L$(CUDA_COMMON_LIB_DIR)
INCLUDE      := -I$(CUDA_COMMON_INC_DIR)
#LIBRARIES    := -lGL -lglut -lGLEW -lpthread
LIBRARIES    := 

$(foreach cc,$(CUDA_ARCH),$(NVCCFLAGS += -gencode arch=compute_$(cc),code=sm_$(cc)))

ifeq ($(ARCH),aarch64)
NVCC_LDFLAGS += -L/usr/lib/aarch64-linux-gnu/tegra
endif


####

all: $(PROG1)

%.o:%.cu
	$(NVCC) $(INCLUDE) $(NVCCFLAGS) -o $@ -c $<

$(PROG1): $(OBJ1)
	$(NVCC) $(NVCC_LDFLAGS) -o $@ $^ $(LIBRARIES)

clean: 
	rm -f $(OBJ1) $(PROG1)

