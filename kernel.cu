
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

#define CUERR(err) if (err != cudaSuccess) { fprintf(stderr, "%s:%d %s\n", \
					__FILE__, __LINE__, cudaGetErrorString(err)); abort(); }


uchar4* uchar3_to_uchar4(const uchar3 *img, size_t w, size_t h)
{
	uchar4 *_img = (uchar4*)malloc(sizeof(uchar4) * w * h);
	if (_img == NULL) return NULL;
	for (int i = 0; i < h; ++i)
		for (int j = 0; j < w; ++j)
			_img[i*w + j] = make_uchar4(img[i*w + j].x, img[i*w + j].y, img[i*w + j].z, 0);
	return _img;
}

uchar4* loadPPM(const char *filename, size_t *width, size_t *height, size_t *maxval)
{
	FILE *fp = NULL;
	char buf[16];

	size_t w=0, h=0, pix_maxval=0;
	uchar3 *_img = NULL;

	if (!(fp = fopen(filename, "rb")))
	{
		fprintf(stderr, "cannot open file \"%s\"\n", filename);
		return NULL;
	}

	// read header "P6"
	if (!fgets(buf, sizeof(buf), fp))
	{
		fprintf(stderr, "failed to read file header\n");
		return NULL;
	}
	if (buf[0] != 'P' && buf[1] != '6')
	{
		fprintf(stderr, "invalid image format\n");
		return NULL;
	}

	// read image size in header
	while (1)
	{
		if (!fgets(buf, sizeof(buf), fp))
		{
			fprintf(stderr, "failed to read file header\n");
			return NULL;
		}
		if (buf[0] != '#') break;
	}
	int ret = sscanf(buf, "%lu %lu", &w, &h);
	if (ret != 2)
	{
		int _ret = sscanf(buf, "%lu\n", &w);
		if (_ret == 0 || _ret == EOF)
		{
			fprintf(stderr, "failed to read file header\n");
			return NULL;
		}

		if (!fgets(buf, sizeof(buf), fp))
		{
			fprintf(stderr, "failed to read file header\n");
			return NULL;
		}
		sscanf(buf, "%lu", &h);
	}

	// read max value in pixel
	while (1)
	{
		if (!fgets(buf, sizeof(buf), fp))
		{
			fprintf(stderr, "failed to read file header\n");
			return NULL;
		}
		if (buf[0] != '#') break;
	}
	sscanf(buf, "%d", &pix_maxval);

	// read image data
	if ((_img = (uchar3*)malloc(sizeof(uchar3) * w * h)) == NULL)
	{
		fprintf(stderr, "failed to allocate memory while loading image data\n");
		return NULL;
	}
	size_t n = fread(_img, sizeof(uchar3) * w, h, fp);
	if (n < h)
	{
		fprintf(stderr, "failed to read data completely\n");
		free(_img);
		return NULL;
	}

	uchar4 *img = uchar3_to_uchar4(_img, w, h);

	fclose(fp);
	free(_img);

	*width = w; *height = h; *maxval = pix_maxval;
	return img;
}

uchar3* uchar4_to_uchar3(const uchar4 *img, size_t w, size_t h)
{
	uchar3 *_img = (uchar3*)malloc(sizeof(uchar3) * w * h);
	if (_img == NULL) return NULL;
	for (int i = 0; i < h; ++i)
		for (int j = 0; j < w; ++j)
			_img[i*w + j] = make_uchar3(img[i*w + j].x, img[i*w + j].y, img[i*w + j].z);
	return _img;
}

bool writePPM(const char* filename, const uchar4 *img, size_t w, size_t h, size_t maxval)
{
	FILE *fp = NULL;

	if (!(fp = fopen(filename, "wb")))
	{
		fprintf(stderr, "cannot open file \"%s\"\n", filename);
		return false;
	}

	// write header to file
	fprintf(fp, "P6\n%d %d\n%d\n", w, h, maxval);
	uchar3 *_img = uchar4_to_uchar3(img, w, h);
	size_t n = fwrite(_img, sizeof(uchar3) * w, h, fp);
	if (n < h)
	{
		fprintf(stderr, "failed to write data\n");
		return false;
	}

	fclose(fp);
	free(_img);

	return true;
}



__global__
void rgbToGrayKernel(uchar4 *dst, const uchar4 *src,
int width, int height, int pitch) {
	int gidx = blockDim.x * blockIdx.x + threadIdx.x;
	int gidy = blockDim.y * blockIdx.y + threadIdx.y;

	if ((gidx < width) && (gidy < height)) {
		int pos = gidx + pitch * gidy;
		uchar4 value = src[pos];
		// Y =  0.299 R + 0.587 G + 0.114 B
		float Y = 0.299f * value.x + 0.587f * value.y + 0.114f * value.z;
		unsigned char y = (unsigned char)min(255, (int)Y);
		dst[pos] = make_uchar4(y, y, y, 0);
	}
}

int divRoundUp(int value, int radix) {
	value += radix - 1;
	return value / radix;
}

int main(void)
{
	uchar4 *src, *dst;
	size_t w, h, maxval;

	// load data and allocate host memory
	src = loadPPM("Mandrill_768x768.ppm", &w, &h, &maxval);
	if (src == NULL) abort();
	dst = (uchar4*)malloc(sizeof(uchar4) * w * h);

	// allocate device memory
	uchar4 *dSrc, *dDst;
	size_t dPitch;
	CUERR(cudaMallocPitch(&dSrc, &dPitch, sizeof(uchar4) * w, h));
	CUERR(cudaMallocPitch(&dDst, &dPitch, sizeof(uchar4) * w, h));

	// memcpy to device
	CUERR(cudaMemcpy2D(dSrc, dPitch, src, sizeof(uchar4) * w,
		sizeof(uchar4) * w, h, cudaMemcpyHostToDevice));

	dim3 blockDim(64, 2);
	dim3 gridDim(divRoundUp(w, blockDim.x),
				divRoundUp(h, blockDim.y));
	rgbToGrayKernel<<<gridDim, blockDim>>>(dDst, dSrc, w, h, dPitch / sizeof(uchar4));
	CUERR(cudaDeviceSynchronize());

	// memcpy to host
	CUERR(cudaMemcpy2D(dst, sizeof(uchar4) * w, dDst, dPitch,
		sizeof(uchar4) * w, h, cudaMemcpyDeviceToHost));

	// write result image
	if (!writePPM("Mandrill_Gray.ppm", dst, w, h, maxval)) abort();

	printf("Pass\n");

	cudaFree(dSrc); cudaFree(dDst);
	free(src); free(dst);
	cudaDeviceReset();
}